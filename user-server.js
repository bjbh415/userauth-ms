'use strict'
const restify = require('restify')
const userModel = require('./users-sequelize')
const util = require('util')

let server  = restify.createServer({
    version: '0.0.1',
    name: 'User-Auth-Service'   
})

server.use(restify.authorizationParser())
server.use(check)
server.use(restify.queryParser())
server.use(restify.bodyParser({
    mapParams: true
}))

//create new user
server.post('/create-user', (req, res, next)=>{
    userModel.create(
        req.params.username,
        req.params.password,
        req.params.provider,
        req.params.familyName,
        req.params.givenName,
        req.params.middleName,
        req.params.emails,
        req.params.photos
    ).then(result=>{
        util.log("User Created", util.inspect(result))
        res.send(result)
        next(false)
    }).catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

//update an existing user record
server.post('/update-user/:username', (req, res, next)=>{
    userModel.update(req.params.username, req.params.password,  
        req.params.provider, req.params.familyName,
        req.params.givenName, req.params.middleName,
        req.params.emails,   req.params.photos)
    .then(result=> {
        util.log('User Updated', util.inspect(result))
        res.send(result)
        next(false)
    }).catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

//find user, if not found create one given profile information
server.post('/find-or-create', (req, res, next)=>{
    userModel.findOrCreate({
        id: req.params.username, username: req.params.username,
        password: req.params.password, provider: req.params.provider,
        familyName: req.params.familyName, givenName: req.params.givenName,
        middleName: req.params.middleName, emails: req.params.emails,
        photos: req.params.photos
    })
    .then(result=>{
        res.send(result)
        next(false)
    })
    .catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

//Find data user (doesn't return password)
server.get('/find/:username', (req, res, next)=>{
    userModel.find(req.params.username)
    .then(user=>{
        if(!user){
            util.log('No user found')
            res.send(404, new Error('No user found'))
        }else{
            res.send(user)
        }
        next(false)
    }).catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

//Delete user record
server.del('/destroy/:username',(req,res, next)=>{
    userModel.destroy(req.params.username)
    .then(()=>{
        util.log('User Deleted')
        res.send({})
        next(false)
    }).catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

//Check password
server.post('/checkPassword', (req, res, next)=>{
    userModel.checkPassword(req.params.username, req.params.password)
    .then(check=>{
        util.log(check)
        res.send(check)
        next(false)
    }).catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

//list users
server.get('/list', (req, res, next)=>{
    userModel.listUsers().then(list=>{
        if(!list) list=[]
        util.log(util.inspect(list))
        res.send(list)
        next(false)
    }).catch(err=>{
        util.log(err.stack)
        res.send(500, err)
        next(false)
    })
})

server.listen(process.env.PORT, 'localhost', ()=>{
    util.log(server.name + ' listening at ' + server.url)
})

let apiKeys = [
    {
        user:"them",
        key:"D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF"
    }
]

function check(req, res, next){
    if(req.authorization){
        let found = false
        for(let auth of apiKeys){
            if(auth.key === req.authorization.basic.password &&
            auth.user === req.authorization.basic.username){
                found = true
                break
            }
        }
        if(found) next()
        else {
            res.send(401, new Error("Not authenticated"))
            util.log('Failed Authentication check' + util.inspect(req.authorization))
            next(false)
        }
    }else{
        console.log('No Authoritation')
        res.send(500, new Error('No Authoritation key'))
        next(false)
    }
}