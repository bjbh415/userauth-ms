'use strict'

const Sequelize = require('sequelize')
const fs = require('fs')
const jsyaml = require('js-yaml')
const util = require('util')

var sequlz
var SQUser

//connectDB
function connectDB(){

    if(SQUser) return SQUser.sync()

    return new Promise((resolve, reject)=>{
        fs.readFile(process.env.SEQUELIZE_CONNECT || 'sequelize-connect.yaml', 'utf8', (err, data)=>{
            if(err) reject(err)
            else resolve(data)
        })
    })

    .then(textyaml=> jsyaml.safeLoad(textyaml, 'utf8'))

    .then(params=>{
        if(!sequlz) sequlz = new Sequelize(params.dbname, params.username, params.password, params.params)
        if(!SQUser){
            SQUser = sequlz.define('Users', {
                username: { type: Sequelize.STRING, unique: true },
                password: Sequelize.STRING,
                provider: Sequelize.STRING,
                familyName: Sequelize.STRING,
                givenName: Sequelize.STRING,
                middleName: Sequelize.STRING,
                emails: Sequelize.STRING(2048),
                photos: Sequelize.STRING(2048)
            })
        }
        return SQUser.sync()
    })
}

//create
exports.create = function(username, password, provider, familyName, giveName, middleName, emails, photos){
    return connectDB().then(SQUser=>{
        return SQUser.create({
            username: username,
            password: password,
            provider: provider,
            familyName: familyName,
            givenName: giveName,
            middleName: middleName,
            emails: JSON.stringify(emails),
            photos: JSON.stringify(photos)
        })
    })
}
//update
exports.update = function(username, password, provider, familyName,
    giveName, middleName, emails, photos){
    return connectDB().then(SQUser=>{
        return SQUser.findOne({where:{username:username}})
            .then(user=>{
                return user? user.updateAttributes({
                    password: password,
                    provider: provider,
                    familyName: familyName,
                    givenName: giveName,
                    middleName: middleName,
                    emails: JSON.stringify(emails),
                    photos: JSON.stringify(photos)
                }) : undefined
            })
    })
}

//find
exports.find = function(username){
    return connectDB().then(SQUser=>{
        return SQUser.findOne({where: {username:username}})
        .then(user=> user ? exports.sanitezedUser(user): undefined)
    })
}

//destroy
exports.destroy = function(username){
    return connectDB().then(SQUser=>{
        return SQUser.findOne({where:{username: username}})
            .then(user=>{
                if(!user) throw new Error('no user found')
                user.destroy()
            })
    })
}

//password checking
exports.passwordCheck = function(username, password){
    return connectDB().then(SQUser=>{
        SQUser.findOne({where:{username: username}})
    })
    .then(user=>{
        if(!user)
            return {check: false, username: username, message: 'no user found'}
        else if(user.username === username && user.password === user.password)
            return {check: true, username: username}
        else
            return {check: false, username: username, message: 'wrong password'}
    })
}

//find one user, if not exists, create it
exports.findOrCreate=function(profile){
    return connectDB().then(SQUser=>{
        return SQUser.findOne(profile.id)
        .then(user => {
            if(user) return user;
            return exports.create(
                profile.id, profile.password,
                profile.provider, profile.familyName,
                profile.givenName, profile.middleName,
                profile.emails, profile.photos
            )
        })
    })
}

//list all users
exports.listUsers = function(){
    return connectDB()
    .then(SQUser=>SQUser.findAll())
    .then(userlist=> users = userlist.map(user=> exports.sanitezedUser(user)))
    .catch(err=>{console.log(err)})
}

//sanitize a user
exports.sanitezedUser = function(user){
    return {
        username: user.username,
        provider: user.provider,
        familyName: user.familyName,
        givenName: user.givenName,
        middleName: user.middleName,
        emails: user.emails,
        photos: user.photos
    }
}